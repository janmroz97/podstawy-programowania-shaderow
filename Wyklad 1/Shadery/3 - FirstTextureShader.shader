﻿Shader "Programowanie shaderow/Wyklad 1/3 - First Texture Shader"
{
	Properties
	{
		_MainTex("Inspector MainTex", 2D) = "white" {}
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			OUT.Emission = tex2D(_MainTex, IN.uv_MainTex);
		}

		ENDCG
	}
}