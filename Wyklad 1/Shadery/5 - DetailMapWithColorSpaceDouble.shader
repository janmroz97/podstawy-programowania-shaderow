﻿Shader "Programowanie shaderow/Wyklad 1/5 - DetailMapWithColorSpaceDouble"
{
	Properties
	{
		_MainTex("Inspector MainTex", 2D) = "white" {}
		_DetailMap("Inspector DetailMap", 2D) = "grey" {}
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _DetailMap;
		float _DetailStrength;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_DetailMap;
		};

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			float3 mainColor = tex2D(_MainTex, IN.uv_MainTex).rgb;
			float detail = tex2D(_DetailMap, IN.uv_DetailMap).r;

			mainColor *= (detail * unity_ColorSpaceDouble);	

			OUT.Emission = mainColor;
		}

		ENDCG
	}
}