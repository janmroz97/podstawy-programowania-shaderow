﻿Shader "Programowanie shaderow/Wyklad 1/2 - WorldPosAsColor"
{
	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		struct Input
		{
			float3 worldPos;
		};

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			OUT.Albedo = IN.worldPos;
		}

		ENDCG
	}
}