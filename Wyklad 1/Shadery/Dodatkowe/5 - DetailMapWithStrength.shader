﻿Shader "Programowanie shaderow/Wyklad 1/Dodatkowe/DetailMapWithStrength"
{
	/*
		Cel:
		Określenie siły detail mapy za pomocą suwaka _DetailStrength.
	*/

	Properties
	{
		_MainTex("Inspector MainTex", 2D) = "white" {}
		_DetailMap("Inspector DetailMap", 2D) = "grey" {}
		_DetailStrength("Detail strength", Range(0, 2)) = 1	//Zwróć uwagę, że nie ma tu żadnych klamr ani śrenika
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _DetailMap;
		float _DetailStrength;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_DetailMap;
		};

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			float3 mainColor = tex2D(_MainTex, IN.uv_MainTex).rgb;
			float detail = tex2D(_DetailMap, IN.uv_DetailMap).r;

			//Manipulowanie siłą detail mapy polega na przeskalowaniu zmiennej detail względem jej śreniej wartości.
			detail -= 0.5; //kolor szary po tym dzialaniu znajdzie sie w zerze
			detail *= _DetailStrength; //rozciąganie przedziału. Kolor szary zostanie w miejscu, kolory ciemniejsze staną się bardziej/mniej ciemne, a jaśniejsze bardziej/mniej jasne.
			detail += 0.5;	//Przywrócenie koloru szarego do pierwotnej pozycji
			detail = saturate(detail); //Ograniczenie wartości detail do przedziału 0-1. Nie chcemy aby do shadera dostał się kolor o ujemnej wartości.

			mainColor *= (detail * 2);	

			OUT.Emission = mainColor;
		}

		ENDCG
	}
}