﻿Shader "DzonyShaderZ/SurfaceWithVertex"
{
	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert vertex:vert

		struct vertexData
		{
			float4 vertex : POSITION;
        	float3 normal : NORMAL;
    		float4 texcoord : TEXCOORD0;
    		float4 texcoord1 : TEXCOORD1;
    		float4 texcoord2 : TEXCOORD2;
		};

		struct Input
		{
			float3 myWorldPos;
		};

		void vert(inout vertexData IN, out Input OUT)
		{
			OUT.myWorldPos = mul(unity_ObjectToWorld, IN.vertex) * 100.0;
			IN.vertex.xyz += IN.normal * sin(_Time.z);
		}

		void surf(Input IN, inout SurfaceOutput OUT)
		{
			OUT.Albedo.xyz = IN.myWorldPos.xyz;
		}

		ENDCG
	}	
}