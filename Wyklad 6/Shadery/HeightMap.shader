﻿Shader "DzonyShaderZ/HeightMap"
{
	Properties
	{
		_MainTex("Albedo", 2D) = "white"
		_HeightMap("HeightMap", 2D) = "grey"
		_HeightStrength("HeightStrength", Float) = 0
		_NormalMap("NormalMap", 2D) = "bump"
		_NormalStrength("NormalStrength", Float) = 1
	}
	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert vertex:vert

		struct vertexData
		{
			float4 vertex : POSITION;
        	float3 normal : NORMAL;
        	float4 tangent : TANGENT;
    		float4 texcoord : TEXCOORD0;
    		float4 texcoord1 : TEXCOORD1;
    		float4 texcoord2 : TEXCOORD2;
		};

		struct Input
		{
			float3 viewDir;
			float2 texcoord;
		};

		void vert(inout vertexData IN, out Input OUT)
		{
			float3x3 objectToTangent = float3x3(IN.tangent.xyz, 
												cross(IN.normal, IN.tangent.xyz),
												IN.normal);

			
			OUT.viewDir = mul(objectToTangent, ObjSpaceViewDir(IN.vertex));
			OUT.texcoord = IN.texcoord;
		}

		sampler2D _MainTex;
		sampler2D _HeightMap;
		float _HeightStrength;
		sampler2D _NormalMap;
		float _NormalStrength;

		void surf(Input IN, inout SurfaceOutput OUT)
		{
			float height = (tex2D(_HeightMap, IN.texcoord) - 0.5) * _HeightStrength;

			float3 viewDir = IN.viewDir;
			viewDir.xy /= viewDir.z;

			float2 uvOffset = height * viewDir.xy;

			float3 norm = UnpackNormal(tex2D(_NormalMap, IN.texcoord + uvOffset));
			float3 col = tex2D(_MainTex, IN.texcoord + uvOffset);

			OUT.Albedo = col;
			OUT.Normal = norm;
		}

		ENDCG
	}	
}