﻿Shader "Programowanie shaderow/Wyklad 4/Pokeball Challenge"
{
	//Uzyj na quadzie

	Properties
	{
		_MainTex("Nothing", 2D) = "black" {}
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf BlinnPhong

		struct Input
		{
			float2 uv_MainTex;
		};

		float4 blendColors(float4 first, float4 second)
		{
			return lerp(first, second, second.a);
		}

		#define RADIUS 0.5f
		#define SMALL_RADIUS 0.15f
		#define STRIP_WIDTH 0.03f

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			float4 colorBuffer = float4(0,0,0,0);

			float2 uv = IN.uv_MainTex;
			float2 center = float2(0.5, 0.5);
			float distanceToCenter = distance(uv, center);

			//Painting colors
			float4 redCircle = float4(1,0,0,0);
			redCircle.a = (distanceToCenter <= RADIUS) ? 1 : 0;
			redCircle.a = (uv.y > 0.5 + STRIP_WIDTH) ? redCircle.a : 0;

			colorBuffer = blendColors(colorBuffer, redCircle);

			float4 whiteCircle = float4(1,1,1,0);
			whiteCircle.a = (distanceToCenter <= RADIUS) ? 1 : 0;
			whiteCircle.a = (uv.y < 0.5 - STRIP_WIDTH) ? whiteCircle.a : 0;

			colorBuffer = blendColors(colorBuffer, whiteCircle);

			float4 blackStrip = float4(0,0,0,0);
			blackStrip.a = (distanceToCenter <= RADIUS) ? 1 : 0;
			blackStrip.a = (uv.y <= 0.5 + STRIP_WIDTH && uv.y >= 0.5 - STRIP_WIDTH) ? blackStrip.a : 0;

			colorBuffer = blendColors(colorBuffer, blackStrip);

			float4 blackSmallCircle = float4(0,0,0,0);
			blackSmallCircle.a = (distanceToCenter <= SMALL_RADIUS + STRIP_WIDTH) ? 1 : 0;

			colorBuffer = blendColors(colorBuffer, blackSmallCircle);

			float4 whiteSmallCircle = float4(1,1,1,0);
			whiteSmallCircle.a = (distanceToCenter <= SMALL_RADIUS - STRIP_WIDTH) ? 1 : 0;

			colorBuffer = blendColors(colorBuffer, whiteSmallCircle);

			//Normals
			float3 norm = lerp(float3(uv - center, 0), float3(0,0,1), pow((0.5-distanceToCenter) * 2, 1));
			OUT.Normal = norm;

			if (colorBuffer.a < 0.01) discard;

			OUT.Albedo = colorBuffer.rgb;
		}

		ENDCG
	}
}