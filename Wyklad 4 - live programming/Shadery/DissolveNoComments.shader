﻿Shader "Programowanie shaderow/Wyklad 4/DissolveNoComments"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_Min("Min height", Float) = 0
		_Max("Max height", Float) = 1
		_Dissolve("Dissolve", Range(0,1)) = 0.5
		_EmissionPow("EmissionPower", Float) = 1
		_EmissionWidth("EmissionWidth", Float) = 1
		_NoiseTex("Noise texture", 2D) = "grey" {}
		_NoiseContrast("Noise contrast", Range(0,1)) = 1
		_EmissionColorUp("Emission up", Color) = (1,1,1,1)
		_EmissionColorDown("Emission down", Color) = (1,1,1,1)
		_EmissionStrength("EmissionStrength", Float) = 1
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		struct Input
		{
			float3 worldPos;
			float2 uv_MainTex;
			float2 uv_NoiseTex;
		};

		sampler2D _MainTex;
		float _Min;
		float _Max;
		float _Dissolve;
		float _EmissionPow;
		float _EmissionWidth;
		sampler2D _NoiseTex;
		float _NoiseContrast;
		fixed4 _EmissionColorUp;
		fixed4 _EmissionColorDown;
		float _EmissionStrength;

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			float3 objPos = mul(unity_WorldToObject, float4(IN.worldPos, 1)).xyz;
			float height = objPos.y;
			float rescaledHeight = (height - _Min) / (_Max - _Min);
			float noise = tex2D(_NoiseTex,  IN.uv_NoiseTex).r;
			float dissolveValue = (rescaledHeight + noise * _NoiseContrast) / (1 + _NoiseContrast);

			if(_Dissolve > dissolveValue) discard;

			float distanceToDissolve = (dissolveValue - _Dissolve) * _EmissionWidth;
			float revDistanceToDissolve = saturate(1 - distanceToDissolve);
			float emi = pow(revDistanceToDissolve, _EmissionPow);
			float3 emissionColor = lerp(_EmissionColorDown, _EmissionColorUp, rescaledHeight);

			float3 col = tex2D(_MainTex, IN.uv_MainTex).rgb;
			OUT.Albedo = col;

			OUT.Emission = emi * emissionColor * _EmissionStrength;
		}

		ENDCG
	}
	Fallback "Diffuse"
}