﻿Shader "Programowanie shaderow/Wyklad 4/Dissolve"
{
	//Dissolve by DzonyShaderZ na poczeby warsztatów. Iprowizorka w 1,5h
	//Pozderka dla wszystkich mordeczek, co pisali ze mną

	//Refki: 
	//https://youtu.be/rqDZN2PDjH8 - DzonyShaderZ
	//https://youtu.be/mkrXZecx9SY - Vacuum Shaders
	//https://assetstore.unity.com/packages/vfx/shaders/advanced-dissolve-111598 - Vacuum Shaders w Asset Store
	//https://youtu.be/vWjvF9UmZXA - Moonflower Carnivore

	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}	//Tekstura, która znajdzie się w albedo
		_Min("Min height", Float) = 0	//Minimalna wysokość punktu na którym pracuje shader
		_Max("Max height", Float) = 1	//Maksymalna wysokość punktu na którym pracuje shader
		_Dissolve("Dissolve", Range(0,1)) = 0.5		//Granica zanikania (0 - cały obiekt, 1 - brak obiektu)
		_EmissionPow("EmissionPower", Float) = 1	//"Kształt" świetlnego paska
		_EmissionWidth("EmissionWidth", Float) = 1	//Szerokość świetlnego paska
		_NoiseTex("Noise texture", 2D) = "grey" {}	//Modyfikator, który doda "plamki"
		_NoiseContrast("Noise contrast", Range(0,1)) = 1	//Kontrast tekstury _NoiseTex, reguluje poszarpanie świetlnej linii
		_EmissionColorUp("Emission up", Color) = (1,1,1,1)	//Kolor świetlnej linii na górze
		_EmissionColorDown("Emission down", Color) = (1,1,1,1)	//Kolor świetlnej linii na dole
		_EmissionStrength("EmissionStrength", Float) = 1	//Siła świecenia linii
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		struct Input
		{
			float3 worldPos;
			float2 uv_MainTex;
			float2 uv_NoiseTex;
		};

		sampler2D _MainTex;
		float _Min;
		float _Max;
		float _Dissolve;
		float _EmissionPow;
		float _EmissionWidth;
		sampler2D _NoiseTex;
		float _NoiseContrast;
		fixed4 _EmissionColorUp;
		fixed4 _EmissionColorDown;
		float _EmissionStrength;

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			//-------------------------------------------------------------------------------------------------------------------
			//---------- Przygotowanie wartosci, na podstawie ktorej bedziemy wykluczac poszczegolne piksele z renderu ----------
			//-------------------------------------------------------------------------------------------------------------------

			//Ludzik ma zanikac od dolu do gory. Aby rozroznic piksele na roznych wysokosciach uzywa sie wspolrzednej y punktu.
			//Nie mozna jednak skorzystac z IN.worldPos, poniewaz shader dla kazdego obiektu bedzie zalezny od miejsca w ktorym sie znajduje.
			//Aby ominac ten problem konwertuje punkt z globalnego ukladu wspolrzednych do lokalnego ukladu wspolrzednych
			//Do konwersji wykorzystuje macierz unity_WorldToObject, ktora po przemnozeniu punktu w globalnych wspolrzednych zwroci punkt lokalnego ukladu.
			float3 objPos = mul(unity_WorldToObject, float4(IN.worldPos, 1)).xyz;
			float height = objPos.y;

			//W shaderach bardzo wygodnie pracuje sie na wartosciach od 0 do 1, bo tylko takie mozna wyswietlic na ekranie
			//Zakres wspolrzednych na ktorych bedzie działać shader jest inny niż 0-1
			//Trzeba we właściwościach shadera określić minimum i maksimum wysokości i zmapować wartości z zakresu [_Min, _Max] na zakres [0, 1]
			//W rescaledHeight bedzia sie znajdowac wartosci od 0 do 1 na ktorych bedzie operowac nasz shader
			float rescaledHeight = (height - _Min) / (_Max - _Min);

			//Pobranie wartosci szumu z tekstury _NoiseTex
			float noise = tex2D(_NoiseTex,  IN.uv_NoiseTex).r;

			//Dodanie otrzymanego szumu do rescaledHeight z uwzglednieniem kontrastu tekstury _NoiseTex
			//Nalezy dodac szum do rescaledHeight po czym podzielić otrzymany wynik przez maksymalną możliwą sumę szumu i rescaledHeight.
			//W przypadku nieuwzględniania kontrastu: float dissolveValue = (rescaledHeight + noise) / 2;
			//Aplikowanie noise można zrealizować na wiele różnych sposobów. Trzeba zawsze mieć na uwadze, że zła aplikacja może zepsuć zakres pracy _Min i _Max. 
			//Zastosowany tutaj sposób aplikacji noise jest najprostszy.
			float dissolveValue = (rescaledHeight + noise * _NoiseContrast) / (1 + _NoiseContrast);

			//Wyłączenie z obliczeń wszystkich pikseli, których dissolveValue jest "ciemniejsze" niż wartość _Dissolve
			if(_Dissolve > dissolveValue) discard;

			//-------------------------------------------------------------------------------------------------------------------
			//------------ Przygotowanie wartosci, ktora określi wygląd świecącego paska przy zanikających pikselach ------------
			//-------------------------------------------------------------------------------------------------------------------

			//Wartością do której najlepiej będzie się odnieśc jest różnica między _Dissolve (granicą zanikania), a aktualnym dissolveValue piksela
			//distanceToDissolve będzie ciemniejsze przy granicy, i jaśniejsze dalej od granicy.
			//_EmissionWidth to zmienna, która posluży do określenia jak wąski ma być świecący pasek.
			float distanceToDissolve = (dissolveValue - _Dissolve) * _EmissionWidth;

			//Zmienna jest odwracana i ograniczana do przedzialu 0-1. 
			//revDistanceToDissolve przyjmuje wartość bliską 1 przy granicy zanikania. Im dalej granicy tym mniejsza wartość.
			float revDistanceToDissolve = saturate(1 - distanceToDissolve);

			//emi to zmienna, w której znajdzie się wygięta funkcja otrzymana w revDistanceToDissolve
			//emi będzie określać siłę świecenia paska przy znikających pikselach
			//revDistanceToDissolve ma wartość z przedziału 0-1. Podniesienie jej do jakiejkolwiek potęgi (0,niesk) nie zmieni jej zbioru wartości. 
			float emi = pow(revDistanceToDissolve, _EmissionPow);

			//Gradient koloru paska przy granicy zanikania
			//rescaledHeight posiada wartości od 0 (przy dole obiektu) do 1 (przy górze).
			//Funkcja lerp płynnie wybierze wartość pomiędzy podanymi dwoma kolorami w zależności od wysokości punktu
			float3 emissionColor = lerp(_EmissionColorDown, _EmissionColorUp, rescaledHeight);

			//-------------------------------------------------------------------------------------------------------------------
			//--------------------------------------- Zaaplikowanie wszystkich zmiennych ----------------------------------------
			//-------------------------------------------------------------------------------------------------------------------

			//Pobranie koloru do Albedo - na to będzie miało wpływ światło
			float3 col = tex2D(_MainTex, IN.uv_MainTex).rgb;
			OUT.Albedo = col;

			//Dodanie paska przy znikających miejscach. Uwzględnienie koloru i siły emisji.
			//Dla _EmissionStrength 0-1 normalny zakres kolorów (LDR - Low Dynamic Range)
			//Dla _EmissionStrength > 1 wysoki zakres kolorów (HDR - High Dynamic Range), wartości niewidoczne na ekranie, ale przetwarzane przez postprocess.
			OUT.Emission = emi * emissionColor * _EmissionStrength;
		}

		ENDCG
	}
	Fallback "Diffuse"
}