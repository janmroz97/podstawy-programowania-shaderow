half4 _SpecColor0;

half4 LightingMyBlinnPhong(SurfaceOutput so, half3 lightDir, half3 viewDir, half atten)
{
	half normalDotLight = saturate(dot(so.Normal, lightDir));

	half3 halfDir = normalize(lightDir + viewDir);
	half normalDotHalf = saturate(dot(halfDir, so.Normal));
	half spec = pow(normalDotHalf, 128 * so.Specular) * so.Gloss;

	half energyConservation = max(_SpecColor0.r, max(_SpecColor0.g, _SpecColor0.b)) * so.Gloss;

	half4 light;
	light.rgb = (1 - energyConservation) * so.Albedo * atten * normalDotLight * _LightColor0;
	light.rgb += atten * spec * _LightColor0 * _SpecColor0;
	light.a = so.Alpha;

	return light;
}