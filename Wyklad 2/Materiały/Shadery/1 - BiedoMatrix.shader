﻿Shader "Programowanie shaderow/Wyklad 2/1 - BiedoMatrix"
{
    Properties
    {
        _MainTex ("Albedo", 2D) = "white" {}
        _WaveCenter("Wave center", Vector) = (0.5, 0.5, 0, 0)
        _WaveStrength("Wave strength", Range(0, 2)) = 0.5
        _WaveDensity("Wave density", float) = 50
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	float2 uv_MainTex;
        };

        sampler2D _MainTex;
        float4 _WaveCenter;
        float _WaveStrength;
        float _WaveDensity;

        void surf(in Input IN, inout SurfaceOutput OUT)
        {
        	float2 vectorFromCenterToUV = _WaveCenter.xy - IN.uv_MainTex;
        	float distanceFromCenter = length(vectorFromCenterToUV);
        	float sinFromDistance = sin(distanceFromCenter * _WaveDensity - _Time.y);
        	float saturatedSin = sinFromDistance * 0.5 + 0.5;
        	float offsetStrength = saturatedSin * _WaveStrength * 0.05;
        	//koniec czesci odpowiedzialnej za natezenie efektu

        	float2 normalizedVectorFromCenter = normalize(vectorFromCenterToUV);
        	float2 uvOffset = normalizedVectorFromCenter * offsetStrength;

        	float3 col = tex2D(_MainTex, IN.uv_MainTex + uvOffset);

        	OUT.Emission = col;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
