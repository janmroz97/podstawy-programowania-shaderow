﻿Shader "Programowanie shaderow/Wyklad 2/4 - DotVisualization"
{
    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	float3 worldNormal;
        	float3 worldPos;
        };

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            float3 view = normalize(_WorldSpaceCameraPos - IN.worldPos);
            float normalDotView = saturate(dot(IN.worldNormal, view));

            OUT.Emission = normalDotView;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
