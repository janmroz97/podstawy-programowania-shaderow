﻿Shader "Programowanie shaderow/Wyklad 2/10 - CustomNormal"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _CustomNormal ("CustomNormal", Vector) = (0,0,1,0)
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	//struct nie moze byc pusty
            float3 worldPos;
        };

        float4 _CustomNormal;
        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
            OUT.Normal = _CustomNormal.xyz;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
