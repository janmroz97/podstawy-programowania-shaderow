﻿Shader "Programowanie shaderow/Wyklad 2/12 - NormalMapWithStrength"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _NormalStrength ("NormalStrength", Range(-2,2)) = 1
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
            float2 uv_NormalMap;
        };

        sampler2D _NormalMap;
        float _NormalStrength;
        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
            float3 norm = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
            OUT.Normal = normalize(float3(norm.xy * _NormalStrength, norm.z));
        }
        ENDCG
    }
    FallBack "Diffuse"
}
