﻿Shader "Programowanie shaderow/Wyklad 2/9 - DefaultNormal"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	//struct nie moze byc pusty
            float3 worldPos;
        };

        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
            OUT.Normal = float3(0,0,1); //To jest domyślna wartość wektora normalnego, więc ta linijka nic nie zmieni
        }
        ENDCG
    }
    FallBack "Diffuse"
}
