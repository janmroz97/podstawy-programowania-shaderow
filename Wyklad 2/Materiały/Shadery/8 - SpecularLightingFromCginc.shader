﻿Shader "Programowanie shaderow/Wyklad 2/8 - SpecularLightingFromCginc"
{
	Properties
	{
		_Color("Albedo color", Color) = (1,1,1,1)
		_SpecColor0("Specular color", Color) = (1,1,1,1)
		_SpecularPower("Specular power", Range(0,1)) = 0.5
		_Gloss("Gloss", Range(0,1)) = 0.5
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf MyBlinnPhong
		#include "./Includes/MyLighting.cginc"

		float4 _Color;
		half _SpecularPower;
		half _Gloss;

		struct Input
		{
			float3 worldPos; //Nie można tego structa zostawić pustego lol
		};

		void surf(in Input IN, inout SurfaceOutput OUT)
		{
			OUT.Albedo = _Color.rgb;
			OUT.Specular = _SpecularPower;
			OUT.Gloss = _Gloss;
		}

		ENDCG
	}
}