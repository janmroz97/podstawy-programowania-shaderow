﻿Shader "Programowanie shaderow/Wyklad 2/14 - SpecularLightingWithTextures"
{
    Properties
    {
    	_MainTex("Albedo", 2D) = "white" {}
    	_SpecularMap("Specular", 2D) = "grey" {}
    	_SpecularStrength ("SpecularStrength", Range(0,1)) = 1
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _NormalStrength ("NormalStrength", Range(-2,2)) = 1
        _SpecColor0 ("Specular Color", Color) = (0.2, 0.2, 0.2, 0)
    }
    SubShader
    {
        CGPROGRAM

        #pragma surface surf MyBlinnPhong
        #include "./Includes/MyLighting.cginc"

        struct Input
        {
            float2 uv_MainTex;
        };

        sampler2D _MainTex;
        sampler2D _SpecularMap;
        sampler2D _NormalMap;
        float _SpecularStrength;
        float _NormalStrength;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
        	OUT.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
        	OUT.Specular = tex2D(_SpecularMap, IN.uv_MainTex).r;
        	OUT.Gloss = OUT.Specular * _SpecularStrength;

            float3 norm = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
            OUT.Normal = normalize(float3(norm.xy * _NormalStrength, norm.z));
        }
        ENDCG
    }
    FallBack "Diffuse"
}
