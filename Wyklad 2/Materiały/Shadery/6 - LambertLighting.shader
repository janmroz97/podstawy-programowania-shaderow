﻿Shader "Programowanie shaderow/Wyklad 2/6 - LambertLighting"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf MyLambert

        half4 LightingMyLambert(SurfaceOutput so, half3 lightDir, half3 viewDir, half atten)
        {
        	half normalDotLight = saturate(dot(so.Normal, lightDir));

        	half4 light;
        	light.rgb = so.Albedo * atten * normalDotLight * _LightColor0;
        	light.a = so.Alpha;

        	return light;
        }

        struct Input
        {
            float3 worldPos;
        };

        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
