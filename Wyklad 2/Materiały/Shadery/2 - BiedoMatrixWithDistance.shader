﻿Shader "Programowanie shaderow/Wyklad 2/2 - BiedoMatrixWithDistance"
{
    Properties
    {
        _MainTex ("Albedo", 2D) = "white" {}
        _WaveCenter("Wave center", Vector) = (0.5, 0.5, 0, 0)
        _WaveStrength("Wave strength", Range(0, 2)) = 0.5
        _WaveDensity("Wave density", float) = 50
        _WaveMaxDistance("Wave max distance", float) = 0.4
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	float2 uv_MainTex;
        };

        sampler2D _MainTex;
        float4 _WaveCenter;
        float _WaveStrength;
        float _WaveDensity;
        float _WaveMaxDistance;

        void surf(in Input IN, inout SurfaceOutput OUT)
        {
        	float2 vectorFromCenterToUV = _WaveCenter.xy - IN.uv_MainTex;
        	float distanceFromCenter = length(vectorFromCenterToUV);
        	float sinFromDistance = sin(distanceFromCenter * _WaveDensity - _Time.y);
        	float saturatedSin = sinFromDistance * 0.5 + 0.5;
        	float distanceModifier = saturate(_WaveMaxDistance - distanceFromCenter);
        	float offsetStrength = saturatedSin * _WaveStrength * 0.05 * distanceModifier;
        	//koniec czesci odpowiedzialnej za natezenie efektu

        	float2 normalizedVectorFromCenter = normalize(vectorFromCenterToUV);
        	float2 uvOffset = normalizedVectorFromCenter * offsetStrength;

        	float3 col = tex2D(_MainTex, IN.uv_MainTex + uvOffset);

        	OUT.Emission = col;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
