﻿Shader "Programowanie shaderow/Wyklad 2/5 - RimEffect"
{
	Properties
	{
		_AlbedoColor("Albedo Color", Color) = (1,1,1,1)
		_RimColor("Rim Color", Color) = (1,1,1,1)
	}
    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        float4 _AlbedoColor;
        float4 _RimColor;

        struct Input
        {
        	float3 worldNormal;
        	float3 worldPos;
        };

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            float3 view = normalize(_WorldSpaceCameraPos - IN.worldPos);
            float normalDotView = saturate(dot(IN.worldNormal, view));

            OUT.Albedo = _AlbedoColor.rgb;
            OUT.Emission = (1 - normalDotView) * _RimColor.rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
