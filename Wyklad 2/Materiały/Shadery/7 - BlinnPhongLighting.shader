﻿Shader "Programowanie shaderow/Wyklad 2/7 - BlinnPhongLighting"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _SpecColor0("SpecColor", Color) = (0.2,0.2,0.2,1)
        _Spec("Spec", Range(0,1)) = 1
        _Gloss("Gloss", Range(0,1)) = 1
    }
    SubShader
    {
        CGPROGRAM

        #pragma surface surf MyBlinnPhong

        half4 _SpecColor0;
        float _Spec;
        float _Gloss;

        half4 LightingMyBlinnPhong(SurfaceOutput so, half3 lightDir, half3 viewDir, half atten)
        {
        	half normalDotLight = saturate(dot(so.Normal, lightDir));

        	half3 halfDir = normalize(lightDir + viewDir);
        	half normalDotHalf = saturate(dot(halfDir, so.Normal));
        	half spec = pow(normalDotHalf, 128 * so.Specular) * so.Gloss;

        	half energyConservation = max(_SpecColor0.r, max(_SpecColor0.g, _SpecColor0.b)) * so.Gloss;

        	half4 light;
        	light.rgb = (1 - energyConservation) * so.Albedo * atten * normalDotLight * _LightColor0;
        	light.rgb += atten * spec * _LightColor0 * _SpecColor0;
        	light.a = so.Alpha;
 
        	return light;
        }

        struct Input
        {
        	//struct nie moze byc pusty
            float3 worldPos;
        };

        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
            OUT.Specular = _Spec;
            OUT.Gloss = _Gloss;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
