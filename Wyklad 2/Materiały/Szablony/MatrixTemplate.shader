﻿Shader "Programowanie shaderow/Wyklad 2/Szablony/MatrixTemplate"
{
    Properties
    {
        _MainTex ("Albedo", 2D) = "white" {}
        _WaveCenter("Wave center", Vector) = (0.5, 0.5, 0, 0)
        _WaveStrength("Wave strength", Range(0, 2)) = 0.5
        _WaveDensity("Wave density", float) = 50
    }

    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	float2 uv_MainTex;
        };

        sampler2D _MainTex;
        float4 _WaveCenter;
        float _WaveStrength;
        float _WaveDensity;

        void surf(in Input IN, inout SurfaceOutput OUT)
        {

        	OUT.Emission.rg = IN.uv_MainTex;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
