﻿Shader "Programowanie shaderow/Wyklad 3/Szablony/SimplestSurface"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        CGPROGRAM

        #pragma surface surf Lambert

        struct Input
        {
        	//struct nie moze byc pusty
            float3 worldPos;
        };

        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput OUT)
        {
            OUT.Albedo = _Color;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
